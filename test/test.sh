#!/bin/bash

set -e

project=$1
ref=$2
url="https://gitlab.com/gitlab-org/security-products/tests/$project.git"

# Clone the test repository.
echo "Cloning the test project $project from $url"
git clone "$url" "/code/$project"

# Run license management on it.
echo "Running license management on the $project with ref $ref"
cd  "/code/$project"
git checkout "$ref" 2> /dev/null
/run.sh analyze .

# In order to upload the artifact to check it manually
mkdir -p /results/
cp "/code/$project/gl-license-management-report.json" "/results/$project-gl-license-management-report.json"

# Compare results with expected results.
diff -u "/code/$project/gl-license-management-report.json" "/test/results/$project.json"
